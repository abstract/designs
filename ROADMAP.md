# Abstract OS & Platform Roadmap

#### Alpha 0.1
    - Business
        1. A trademark application for Abstract OS, Abstract Studio, and Abstract Platform.
        1. A Contributor Guide for our Repos.
        1. A Code of Conduct for our Repos - Narrow in scope to only actions within our community.
    - Abstract OS
        1. A desktop environment based on elementaryOS's Wingpanel, LightDM, Gala, and Dock.
        1. A build process that produces x64 and 32 bit image files to flash in a VM.
        1. A GTK 3.0/4.0 CSS Theme
    - Applications (All MVPs)
        1. Trail Marker - Short name "Marker" - This is a notes+markdown application that allows users to write notes in markdown.
        1. Register - A Basic text editor for Abstract OS.
        1. Review - An application that allows quick previews of many file types (Images, PDFs, Text Files, etc).
    - Developer Evangelism
        1. An MVP of Vala Welder that allows compiling of Vala Applications.
        1. A documented development process using Vala + Vala Welder. 
        1. An Abstract Software Library that Applications can utilize. It should include GTK and all libraries used to build Abstract OS Applications.
        1. A GitLab CI/CD Pipeline to build Abstract OS Applications.
        1. A GitLab CI/CD Pipeline that can produce AppImage's
        1. A GitLab CI/CD Pipeline that can produce Alpine APKs.
        1. A GitLab CI/CD Project Template for Developers to use.
        1. An Alpine Repository
    - Marketing
        1. A Finished Abstract Studio website.
        1. A Design for the Abstract OS Website.
    - Design
        1. A Mockup design of the future Desktop Environment.
        1. A Mockup design of the File Manager Application.
        1. A Design for the Abstract GTK Theme

#### Alpha 0.2
    - Abstract OS
        1. An ARM Build process that produces .img files to flash.
    - Applications (All MVPs)
    - Design
        1. An Installer GUI Design complete and signed off.
        1. A First-Run GUI to walk users through Abstract OS, and get their first account provisioned.
    - Developer Evangelism
        1. A GitLab CI/CD Pipeline that can unit test/test Abstract OS/Vala Applications.
        1. A documented guide on what makes an "Abstact OS Application" different.
        1. The MVP for Development Guidelines/Standards for Abstract OS Applications.
    - Abstract Studio
        1. A Go-To-Market Document outlining the Abstract Studio Applications; What they do, their name, functionality.
        1. A GitLab CI/CD Pipeline that builds, tests, and releases AStudio Applications.

#### Alpha 0.3
    - Abstract OS
        1. A build process that produces ARM, X86, 32BIT .img and .iso files to flash onto machines.
        1. An install process that installs Refind on EFI/GPT Machines
        1. An install process that installs GRUB on non-EFI/GPT Machines.
